#include "Mesh.h"
#include "Sensors.h"

#define UPDATE_INTERVAL_MINUTES 10
#define DEBUG

//Mesh library
MyTransportNRF24 transport(RF24_CE_PIN, RF24_CS_PIN, RF24_PA_LEVEL_GW);
MyHwATMega328 hw;
MySensor node(transport, hw);

void setup() {  
  initMeshNetwork(&node);
  initSensors();
}

void loop() {
  node.process(); //Process message's
  //sendUpdate(); //Try to send an update to the master node


 // if(!MESH_DO_REPEAT){ //Only sleep if this is not a repeating node !
   //  node.sleep(UPDATE_INTERVAL_MINUTES * 60 * 1000); //Let the node sleep for a while. 
  //}
}
