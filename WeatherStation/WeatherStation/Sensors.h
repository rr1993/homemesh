#pragma once
#include <stdint.h>
#include <Arduino.h>

#define LDR_PIN A7
#define CO2_PIN A0
#define HUMIDITY_PIN 8


/**
 * Defines a sensor, and all it values
 */
typedef struct{
  int aSensorPin; //Pin where the sensor is connected at
  uint16_t preparationTime; //Time it takes for the sensor to become ready (warm-up time)
  bool isActive; //Flag to determine if the sensor is active, the master-node can turn some sensors of to conserve energy
}Sensor;

/**
 * @brief Init the sensors
 */
void initSensors();

/**
 * @brief Put the sensors in standby mode to save energy
 */
void standbySensors();

/**
 * @brief Wake up the sensors after a while sleeping
 */
void wakeUpSensors();


/**
 * @brief Check if the sensors are ready to be used 
 * 
 * @return Returns true if the sensors are ready to be used, false otherwise
 */
bool sensorsReady();

 /**
  * @brief Get the current lightLevel
  */
uint16_t getLightLevel();

 /**
  * @brief Get the current temperature
  */
float getCurrentTemperature();

 /**
  *  @brief Get the current air-pressure
  */ 
float getAirPressure();

  /**
   * @brief Get the current C02 level
   */
uint16_t getTheCurrentCO2Level();


/**
 * @brief Get the current battery level
 */
uint16_t getBatteryLevel();
