#include "Sensors.h"

void initSensors(){
  pinMode(LDR_PIN, INPUT);
  pinMode(CO2_PIN, INPUT);
}

uint16_t getLightLevel(){
  return analogRead(LDR_PIN);
}

uint16_t getCurrentCO2Level(){
  return analogRead(CO2_PIN);
}

bool sensorsReady(){
  return true; //TODO
}

