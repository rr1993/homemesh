#pragma once

#include<SPI.h>
#include<MySensor.h>

#define MESH_NODE_ID 10
#define MESH_DO_REPEAT false
#define RF24_CE_PIN 10
#define RF24_CS_PIN 9

/**
 * @brief Init the mesh library
 */
 void initMeshNetwork(MySensor *theSensor);


/**
 * @brief Try to send an update to the gateway
 */
 void sendUpdate();
